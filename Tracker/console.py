#!/usr/bin/python3
#from src.repository import Repository
from src import Repository
from src import Priority
import argparse
import sys


def task_add_handle(args, x):
    try:
        x.run()
    except ValueError as e:
        print(e)
        sys.exit(-1)
    try:
        x.add_task_to_user(args.name, task_priority=args.urgency, task_interval=args.interval,
                           task_parent_id=args.parent, task_start_time=args.start, task_end_time=args.end,
                           task_due_date=args.due, task_days=args.days)
        print("Task added")
    except (ValueError, NameError) as e:
        print(e)


def task_done_handle(args, x):
    try:
        x.run()
    except ValueError as e:
        print(e)
        sys.exit(-1)

    try:
        x.complete_user_task(args.i)
        print("Task done")
    except (ValueError, NameError) as e:
        print(e)
        sys.exit(1)


def task_edit_handle(args, x):
    try:
        x.run()
    except ValueError as e:
        print(e)
        sys.exit(-1)

    try:
        x.edit_task(args.id, project_id=args.p, name=args.n, priority=args.u, interval=args.i)
    except NameError as e:
        print(e)
        sys.exit(1)


def task_move_handle(args, x):
    try:
        x.run()
    except ValueError as e:
        print(e)
        sys.exit(-1)

    try:
        x.move_task(args.source, args.destination, args.p)
    except ValueError as e:
        print(e)
        sys.exit(1)


def task_remove_handle(args, x):
    try:
        x.run()
    except ValueError as e:
        print(e)
        sys.exit(-1)

    try:
        x.remove_task(args.id)
    except ValueError as e:
        print(e)
        sys.exit(1)


def project_create_handle(args, x):
    try:
        x.run()
    except ValueError as e:
        print(e)
        sys.exit(-1)

    try:
        x.create_project(args.name)
    except (ValueError, NameError) as e:
        print(e)
        sys.exit(1)


def project_task_done_handle(args, x):
    try:
        x.run()
    except ValueError as e:
        print(e)
        sys.exit(-1)

    try:
        x.complete_user_task(args.task_id, project_id=args.project_id)
    except (ValueError, NameError) as e:
        print(e)
        sys.exit(1)


def project_add_task_handle(args, x):
    try:
        x.run()
    except ValueError as e:
        print(e)
        sys.exit(-1)

    try:
        x.add_task_to_user(args.name, project_id=args.project_id, task_priority=args.urgency, task_interval=args.interval,
                           task_parent_id=args.parent, task_start_time=args.start, task_end_time=args.end,
                           task_due_date=args.due)
    except (ValueError, NameError) as e:
        print(e)
        sys.exit(1)


def project_remove_task_handle(args, x):
    try:
        x.run()
    except ValueError as e:
        print(e)
        sys.exit(-1)

    try:
        x.remove_task(args.task_id, project_id=args.project_id)
    except ValueError as e:
        print(e)
        sys.exit(1)


def project_invite_handle(args, x):
    try:
        x.run()
    except ValueError as e:
        print(e)
        sys.exit(-1)

    try:
        x.invite_users(args.project_id, args.user)
    except (ValueError, NameError) as e:
        print(e)
        sys.exit(1)


def user_handle(args, x):
    try:
        x.run()
    except ValueError as e:
        print(e)
        sys.exit(-1)

    try:
        if args.add is not None:
            x.add_user(args.add)
        elif args.remove is not None:
            x.remove_user(args.remove)
        elif args.switch is not None:
            x.switch_user(args.switch)
        elif args.show:
            u = [str(x) for x in x.users.values()]
            s = "\n".join(["All users:", *u])
            print(s)
            print("Current user:\n", x.current_user, sep="")
    except NameError as e:
        print(e)
        sys.exit(1)


def project_handle(args, x):
    x.complete_user_task(args.task_id, project_id=args.task_id)


def project_move_task_handle(args, x):
    try:
        x.run()
    except ValueError as e:
        print(e)
        sys.exit(-1)

    try:
        if args.back:
            x.move_task_from_project(args.project_id, args.task_id)
        else:
            x.move_task_to_project(args.task_id, args.project_id)
    except (NameError, ValueError) as e:
        print(e)
        sys.exit(1)


def print_tasks(args, x):
    try:
        x.run()
    except ValueError as e:
        print(e)
        sys.exit(-1)

    print("Common tasks:")
    x.print_tasks()
    if args.done is True:
        print("Done tasks:")
        x.print_done_tasks()
    if args.failed is True:
        print("Failed tasks:")
        x.print_failed_tasks()
    if args.projects:
        print("Your projects:")
        x.print_projects()


def default(args, x):
    print("Ask for help")
    sys.exit(0)

def repo_init_handle(args, x):
    x.init_repo(args.name)


def main():
    x = Repository()
    #try:
    #    x.run()
    #except ValueError as e:
    #    print(e)
    #    sys.exit(-1)

    parser = argparse.ArgumentParser()
    parser.set_defaults(func=default)

    subparsers = parser.add_subparsers()

    init_parser = subparsers.add_parser("init")
    init_parser.add_argument("name", help="Enter your name to init repository")
    init_parser.set_defaults(func=repo_init_handle)

    task_parser = subparsers.add_parser("task")

    task_subparsers = task_parser.add_subparsers()
    add_task_parser = task_subparsers.add_parser("add")
    add_task_parser.add_argument("name", help="Task name")
    add_task_parser.add_argument("-p", "--parent", help="Add new subtask to parent", default=None)
    add_task_parser.add_argument("-u", "--urgency", choices=["low", "medium", "high"],
                                 help="Task urgency (low, medium, high)", default=Priority.Medium)
    add_task_parser.add_argument("-d", "--due", help="Set due date", default=None)
    add_task_parser.add_argument("-i", "--interval",
                                 help="Make task recurring setting it's interval "
                                 "(hourly, daily, monthly, yearly", default=None)
    add_task_parser.add_argument("-e", "--end", help="Set end time, useful for recurring tasks", default=None)
    add_task_parser.add_argument("-s", "--start", help="Set start time", default=None)
    add_task_parser.add_argument("--days", help="Set recurrence days")
    add_task_parser.set_defaults(func=task_add_handle)

    show_task_parser = task_subparsers.add_parser("show")
    show_task_parser.add_argument("-f", "--failed", action="store_true", help="Show failed tasks")
    show_task_parser.add_argument("-d", "--done", action="store_true", help="Show done tasks")
    show_task_parser.add_argument("-p", "--projects", action="store_true", help="Show projects")
    show_task_parser.set_defaults(func=print_tasks)

    move_task_parser = task_subparsers.add_parser("move")
    move_task_parser.add_argument("source", help="What to move (id)")
    move_task_parser.add_argument("-d", "--destination", help="Where to move (id)")
    move_task_parser.add_argument("-p", help="Move tasks inside project")
    move_task_parser.set_defaults(func=task_move_handle)

    edit_task_parser = task_subparsers.add_parser("edit")
    edit_task_parser.add_argument("id", help="Task ID")
    edit_task_parser.add_argument("-p", help="Change task inside project")
    edit_task_parser.add_argument("-n", help="Change task name")
    edit_task_parser.add_argument("-u", choices=["low", "medium", "high"], help="Change task urgency",
                                  default=Priority.Medium)
    edit_task_parser.add_argument("-i", help="Change task interval")
    edit_task_parser.set_defaults(func=task_edit_handle)

    task_done_parser = task_subparsers.add_parser("done")
    task_done_parser.add_argument("i", help="Done task ID")
    task_done_parser.set_defaults(func=task_done_handle)

    task_remove_parser = task_subparsers.add_parser("remove")
    task_remove_parser.add_argument("id", help="Task id to remove")
    # task_remove_parser.add_argument("-p", help="Remove task in project")
    task_remove_parser.set_defaults(func=task_remove_handle)

    user_parser = subparsers.add_parser("user")
    user_parser.add_argument("-a", "--add", help="Add new user")
    user_parser.add_argument("-r", "--remove", help="Remove user")
    user_parser.add_argument("--switch", help="Enter your user-name")
    user_parser.add_argument("--show", action="store_true", help="Show all users")
    user_parser.set_defaults(func=user_handle)

    project_parser = subparsers.add_parser("project")
    project_subparsers = project_parser.add_subparsers()
    add_project_parser = project_subparsers.add_parser("create")
    add_project_parser.add_argument("name", help="Project Name")
    add_project_parser.set_defaults(func=project_create_handle)

    invite_users_parser = project_subparsers.add_parser("invite")
    invite_users_parser.add_argument("project_id", help="Project id")
    invite_users_parser.add_argument("user", help="Invite more users")
    invite_users_parser.set_defaults(func=project_invite_handle)

    project_done_task = project_subparsers.add_parser("done")
    project_done_task.add_argument("project_id", help="Project id")
    project_done_task.add_argument("task_id", help="Task id")
    project_done_task.set_defaults(func=project_task_done_handle)

    project_move_task = project_subparsers.add_parser("move")
    project_move_task.add_argument("project_id", help="Project id")
    project_move_task.add_argument("task_id", help="Task id")
    project_move_task.add_argument("--back", action="store_true", help="Move task back from project to user tasks")
    project_move_task.set_defaults(func=project_move_task_handle)

    project_add_task = project_subparsers.add_parser("add")
    project_add_task.add_argument("project_id", help="Project id")
    project_add_task.add_argument("name", help="Task name")
    project_add_task.add_argument("-p", "--parent", help="Add new subtask to parent", default=None)
    project_add_task.add_argument("-u", "--urgency", choices=["low", "medium", "high"],
                                  help="Task urgency (low, medium, high)", default=Priority.Medium)
    project_add_task.add_argument("-d", "--due", help="Set due date", default=None)
    project_add_task.add_argument("-i", "--interval",
                                  help="Make task recurring setting it's interval "
                                  "(hourly, daily, monthly, yearly", default=None)
    project_add_task.add_argument("-e", "--end", help="Set end time, useful for recurring tasks", default=None)
    project_add_task.add_argument("-s", "--start", help="Set start time", default=None)
    project_add_task.set_defaults(func=project_add_task_handle)

    project_remove_task = project_subparsers.add_parser("remove")
    project_remove_task.add_argument("project_id", help="Where to remove task")
    project_remove_task.add_argument("task_id", help="")
    project_remove_task.set_defaults(func=project_remove_task_handle)

    args = parser.parse_args()
    # (args)

    args.func(args, x)

    x.stop()


if __name__ == '__main__':
    main()
