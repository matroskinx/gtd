import unittest
from src import Repository
from src import Task
import itertools



class TestRepository(unittest.TestCase):
    def setUp(self):
        self.x = Repository()
        self.x.add_user("User")

    def tearDown(self):
        Task.idGen = itertools.count(start=1)

    def test_add_user(self):
        uid = self.x.add_user("Ivan")
        self.assertIn(uid, self.x.users)

    def test_remove_user(self):
        self.x.add_user("Vlad")
        uid = self.x.remove_user("Vlad")
        self.assertNotIn(uid, self.x.users)

        with self.assertRaises(NameError):
            self.x.remove_user("NNN")

    def test_add_task(self):
        with self.assertRaises(ValueError):
            self.x.add_task_to_user("Go to shop", task_interval="daily")

        with self.assertRaises(ValueError):
            self.x.add_task_to_user("Task", task_due_date="01/01/2000")

        with self.assertRaises(NameError):
            self.x.add_task_to_user("Task two", task_parent_id="abc")

        id = self.x.add_task_to_user("Simple task")
        self.assertIn(id, self.x.repo[self.x.current_user.uid].all_tasks)

    def test_complete_task(self):
        with self.assertRaises(ValueError):
            self.x.complete_user_task("abc")

    def test_switch_user(self):
        self.x.add_user("Ivan")
        self.x.switch_user("User")
        self.assertEqual(self.x.current_user.name, "User")

    def test_create_project(self):
        pid = self.x.create_project("Test")
        p = self.x.get_project(pid)
        self.assertIn(p, self.x.projects)

    def test_collect_task(self):
        self.x.add_task_to_user("First") #1
        self.x.add_task_to_user("First sub", task_parent_id="1") #2
        self.x.add_task_to_user("Second sub", task_parent_id="1") #3
        self.x.add_task_to_user("Third sub", task_parent_id="1") #4
        self.x.add_task_to_user("FF sub", task_parent_id="2") #5
        self.x.add_task_to_user("FS sub", task_parent_id="2") #6
        self.x.add_task_to_user("FFF sub", task_parent_id="5")

        uid = self.x.current_user.uid

        cnt = self.x.repo[uid].all_tasks

        t = sorted(self.x.__collect_tasks__("1", cnt))

        result = ["1", "2", "3", "4", "5", "6", "7"]
        self.assertListEqual(t, result)

    def test_move_task(self):
        self.x.add_task_to_user("First")
        self.x.add_task_to_user("First sub", task_parent_id="1") #2
        self.x.add_task_to_user("Second sub", task_parent_id="1") #3
        self.x.add_task_to_user("Third sub", task_parent_id="1") #4
        self.x.move_task("4", None)

        uid = self.x.current_user.uid
        cnt = self.x.repo[uid].all_tasks
        self.assertNotIn("4", cnt["1"].subtasks)

    def test_move_fail(self):
        self.x.add_task_to_user("First")
        with self.assertRaises(ValueError):
            self.x.move_task("1", "1")


if __name__ == "__main__":
    unittest.main()