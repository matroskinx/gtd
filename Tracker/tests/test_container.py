import unittest
from src import Container
from src import Task
import itertools


class TestContainer(unittest.TestCase):

    def setUp(self):
        self.task1 = Task("Morning")
        self.task2 = Task("Evening")
        self.task3 = Task("Wake up", parent_id="1")
        self.task4 = Task("Turn off alarm", parent_id="3")
        self.task5 = Task("Go to shower", parent_id="1")
        self.task6 = Task("Fake task", parent_id="66")
        self.cnt = Container()

    def tearDown(self):
        Task.idGen = itertools.count(start=1)

    def test_add_task(self):
        self.cnt.container_add_task(self.task1)
        self.assertIn(self.task1, self.cnt.all_tasks.values())

        self.cnt.container_add_task(self.task2)
        self.assertIn(self.task2, self.cnt.all_tasks.values())

        self.cnt.container_add_task(self.task3)
        self.assertIn(self.task3, self.cnt.all_tasks.values())

        self.cnt.container_add_task(self.task4)
        self.assertIn(self.task4, self.cnt.all_tasks.values())

        self.cnt.container_add_task(self.task5)
        self.assertIn(self.task5, self.cnt.all_tasks.values())

        with self.assertRaises(NameError):
            self.cnt.container_add_task(self.task6)

    def test_complete_task(self):
        self.cnt.container_add_task(self.task1)
        self.cnt.container_add_task(self.task2)
        self.cnt.container_add_task(self.task3)
        self.cnt.container_add_task(self.task4)
        self.cnt.container_add_task(self.task5)

        self.cnt.mark_as_completed("1")

        self.assertIn(self.task1.id, self.cnt.completed_tasks)
        self.assertIn(self.task3.id, self.cnt.completed_tasks)
        self.assertIn(self.task4.id, self.cnt.completed_tasks)
        self.assertIn(self.task5.id, self.cnt.completed_tasks)
        self.assertNotIn(self.task1.id, self.cnt.all_tasks)
        self.assertNotIn(self.task3.id, self.cnt.all_tasks)
        self.assertNotIn(self.task4.id, self.cnt.all_tasks)
        self.assertNotIn(self.task5.id, self.cnt.all_tasks)

        with self.assertRaises(ValueError):
            self.cnt.mark_as_completed("66")

    def test_remove_task(self):
        self.cnt.container_add_task(self.task1)
        self.cnt.container_add_task(self.task2)
        self.cnt.container_add_task(self.task3)
        self.cnt.container_add_task(self.task4)
        self.cnt.container_add_task(self.task5)

        self.cnt.container_remove_task("1")

        self.assertNotIn(self.task1, self.cnt.all_tasks.values())
        self.assertNotIn(self.task3, self.cnt.all_tasks.values())
        self.assertNotIn(self.task4, self.cnt.all_tasks.values())
        self.assertNotIn(self.task5, self.cnt.all_tasks.values())

    def test_remove_subtask(self):
        t = Task("Parent")
        t2 = Task("Child", parent_id=t.id)
        self.cnt.container_add_task(t)
        self.cnt.container_add_task(t2)
        self.cnt.container_remove_task(t2.id)

        self.assertNotIn(t2.id, t.subtasks)



if __name__ =="__main__":
    unittest.main()



