import unittest
from src import utils
from dateutil.relativedelta import *


class TestTask(unittest.TestCase):
    pass
    def test_recurrence(self):
        d = utils.get_recurrence_days("mon wed tue")
        rd = [0, 1, 2]
        self.assertListEqual(d, rd)

    def test_recurence_fail(self):
        with self.assertRaises(ValueError):
            utils.get_recurrence_days("abcd")

    def test_time_delta(self):
        delta = utils.get_time_delta("hourly")
        real_delta = relativedelta(hours=1)
        self.assertEqual(delta, real_delta)

    def test_time_delta_fail(self):
        with self.assertRaises(ValueError):
            utils.get_time_delta("nnnn")

    def test_interval(self):
        delta = utils.get_time_delta("3h")
        real_delta = relativedelta(hours=3)
        self.assertEqual(delta, real_delta)
