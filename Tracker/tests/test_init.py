import unittest
import os
from src import Repository
import shutil
from src import Task
import itertools



class TestRepository(unittest.TestCase):
    @classmethod
    def tearDownClass(cls):
        pass
        cls.clean_everything(cls)

    @classmethod
    def setUpClass(cls):
        cls.x = Repository("test_config")

    def setUp(self):
        self.x.init_repo("USER")

    def test_run(self):
        p = os.path.join(self.x.settings.folder, self.x.settings.users)
        os.remove(p)
        with self.assertRaises(FileNotFoundError):
            self.x.run()

    def test_empty_run(self):
        p = os.path.join(self.x.settings.folder, self.x.settings.users)
        with open(p, "w") as file:
            file.write("")

        with self.assertRaises(ValueError):
            self.x.run()

    def clean_everything(self):
        if os.path.exists(self.x.settings.folder):
            shutil.rmtree(self.x.settings.folder)
        if os.path.exists(self.x.settings.config_path):
            os.remove(self.x.settings.config_path)
        if os.path.exists("log.log"):
            os.remove("log.log")
