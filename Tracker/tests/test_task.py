import unittest
from src import Task, State, Priority
from datetime import datetime
from datetime import timedelta

import time


class TestTask(unittest.TestCase):

    def test_overdue(self):
        due = datetime.now() + timedelta(seconds=2)
        t = Task(name="Testing", due_date=due)
        t.task_mark_overdue()
        self.assertEqual(t.state, State.Active)
        time.sleep(3)
        t.task_mark_overdue()
        self.assertEqual(t.state, State.Overdue)

    def test_rearange(self):
        due = datetime.now() + timedelta(hours=3)
        interval = timedelta(hours=1)
        t = Task(name="abc", due_date=due, interval=interval)
        t.task_rearrange()
        self.assertEqual(due+interval, t.due_date)

    def test_state(self):
        due = datetime.now() + timedelta(hours=3)
        interval = timedelta(hours=1)
        t = Task(name="abc", due_date=due, interval=interval)
        self.assertEqual(State.Recur, t.state)

    def test_state_ended(self):
        due = datetime.now() + timedelta(seconds=1)
        interval = timedelta(hours=1)
        end = datetime.now() + timedelta(seconds=5)
        t = Task(name="abc", due_date=due, interval=interval, end_time=end)
        t.task_rearrange()
        t.task_check_end()
        self.assertEqual(State.Ended, t.state)

    def test_edit(self):
            t = Task(name="abc")
            t.task_edit(name="newName")
            self.assertEqual(t.name, "newName")

    def test_defaults(self):
        t = Task(name="abc")
        self.assertEqual(t.priority, Priority.Medium)
        self.assertEqual(t.state, State.Active)

if __name__ == "__main__":
    unittest.main()