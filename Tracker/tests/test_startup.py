import unittest
from src import Repository
from src import User
import os
import shutil
import itertools


class TestStartup(unittest.TestCase):
    @classmethod
    def tearDownClass(cls):
        pass
        cls.clean_everything(cls)

    @classmethod
    def setUpClass(cls):
        cls.x = Repository("test_config")
        cls.x.init_repo("USER")
        User.uidGen = itertools.count(start=1)

    def test_init(self):
        self.assertTrue(os.path.exists(self.x.settings.folder))


    def test_file(self):
        self.x.run()
        id = self.x.add_task_to_user("New task")
        self.x.stop()

        self.x.run()
        self.assertIn(id, self.x.repo[self.x.current_user.uid].all_tasks)
        self.x.complete_user_task(id)
        self.x.stop()

        self.x.run()
        self.assertNotIn(id, self.x.repo[self.x.current_user.uid].all_tasks)
        self.x.stop()

    def test_user_switch(self):
        self.x.run()
        self.x.add_user("NEW")
        self.x.stop()

        self.x.run()
        n = self.x.current_user.name
        self.assertEqual(n, "NEW")

    def clean_everything(self):
        if os.path.exists(self.x.settings.folder):
            shutil.rmtree(self.x.settings.folder)
        if os.path.exists(self.x.settings.config_path):
            os.remove(self.x.settings.config_path)
        if os.path.exists("log.log"):
            os.remove("log.log")


if __name__ == "__main__":
    unittest.main()