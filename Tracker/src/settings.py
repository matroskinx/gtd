import configparser
import os


class Settings:
    def __init__(self, config_name=None):
        if not config_name:
            self.config_path = "config.ini"
        else:
            self.config_path = ".".join((config_name, "ini"))
        self.folder = str(Settings.get_setting('FILES', 'data_folder', self.config_path))
        self.projects = str(Settings.get_setting('FILES', 'projects', self.config_path))
        self.repository = str(Settings.get_setting('FILES', 'repository', self.config_path))
        self.users = str(Settings.get_setting('FILES', 'users', self.config_path))
        self.current_user = str(Settings.get_setting('FILES', 'current_user', self.config_path))
        self.ids = str(Settings.get_setting('FILES', 'ids', self.config_path))


    @staticmethod
    def create_config(config_file):
        config = configparser.ConfigParser()
        config.add_section("FILES")
        config.set("FILES", "data_folder", "data")
        config.set("FILES", "projects", "proj.json")
        config.set("FILES", "repository", "repo.json")
        config.set("FILES", "users", "users.json")
        config.set("FILES", "current_user", "currentUsr.json")
        config.set("FILES", "ids", "ids.dat")

        with open(config_file, "w") as config_file:
            config.write(config_file)

    @staticmethod
    def get_config(config_file):

        if not os.path.exists(config_file):
            Settings.create_config(config_file)

        config = configparser.ConfigParser()
        config.read(config_file)
        return config

    @staticmethod
    def get_setting(section, setting, config_file):
        config = Settings.get_config(config_file)
        value = config.get(section, setting)
        return value

