from src import Task
from src import Container
import itertools


class Project():
    pidGen = itertools.count(start=1)

    def __init__(self, name, creator_id):
        self.name = name
        self.creator_id = creator_id
        self.user_ids = []
        self.pid = str(next(self.pidGen))
        self.storage = Container()


    def __str__(self):
        s = " | ".join([self.pid, self.name])
        return s

