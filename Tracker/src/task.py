from datetime import datetime
import itertools
from dateutil.parser import *
from dateutil.relativedelta import *


class State():
    Active = "Active"
    Recur = "Recur"
    Done = "Done"
    Overdue = "Overdue"
    Ended = "Ended"


class Priority():
    Low = "low"
    Medium = "medium"
    High = "high"


class Task:
    """Represents a simple task"""
    idGen = itertools.count(start=1)

    def __init__(self, name, priority=Priority.Medium, interval=None, parent_id=None,
                 start_time=None, end_time=None, due_date=None, recur_days=None):
        self.id = str(next(self.idGen))
        self.name = name
        self.description = None
        self.priority = priority
        self.interval = interval
        self.recur_days = recur_days
        if self.interval and self.recur_days:
            raise ValueError("Choose between recurrence days and interval")

        if interval or recur_days:
            self.recurring = True
            self.state = State.Recur
        else:
            self.recurring = False
            self.state = State.Active
        self.subtasks = []
        self.parent_id = parent_id
        self.due_date = due_date
        if start_time is None:
            self.start_time = datetime.now()
        else:
            self.start_time = start_time
        self.end_time = end_time

    def task_complete(self):
        self.state = State.Done

    def task_mark_overdue(self):    # проверить несколько пропущенных дедлайнов подряд
        """
        Return true if task is overdue
        Postpone recurring task
        """
        if self.due_date:
            if datetime.now() > self.due_date:
                self.state = State.Overdue
                if self.end_time:
                    if datetime.now > self.end_time or self.due_date > self.end_time:
                        self.state = State.Ended
                        self.recurring = False
                #if self.end_time is not None and datetime.now() > self.end_time or self.due_date > self.end_time:
                #    self.state = State.Ended
                #    self.recurring = False
                return True
        else:
            return False

    def task_check_end(self):
        if self.end_time:
            if self.due_date > self.end_time or datetime.now() > self.end_time:
                self.state = State.Ended
                self.recurring = False

    def task_rearrange(self):   #modified
        if self.interval:
            self.due_date = self.due_date + self.interval
            while self.due_date < datetime.now():
                self.due_date = self.due_date + self.interval

        elif self.recur_days:
            self.due_date = self.due_date + relativedelta(days=1)
            print(self.due_date.weekday())
            while self.due_date.weekday() not in self.recur_days or self.due_date < datetime.now():
                self.due_date += relativedelta(days=1)

        if self.state != State.Ended:
            self.state = State.Recur

    def task_edit(self, name=None, priority=None, interval=None):
        if name:
            self.name = name
        if priority:
            self.priority = priority
        if interval:
            if self.interval:
                self.interval = interval

    def __str__(self):
        s = " | ".join([self.id, self.name, str(self.priority)])

        if len(self.subtasks) != 0:
            s = " | ".join([s, "Subs: " + str(self.subtasks)])

        s = " | ".join([s, self.state])
        if self.due_date:
            s = " | ".join([s, "Due: " + str(self.due_date)])

        return s

