import copy


class Container:
    """
    Container class stores dicts with pairs {task id: task} for all tasks, completed tasks
    and failed tasks.
    """
    def __init__(self):
        self.all_tasks = {}
        self.completed_tasks = {}
        self.failed_tasks = {}

    def container_add_task(self, task):
        self.all_tasks[task.id] = task
        parent_id = task.parent_id

        if parent_id:
            if parent_id in self.all_tasks:
                self.all_tasks[parent_id].subtasks.append(task.id)
            else:
                raise NameError("There is no {} parent".format(parent_id))

    def container_remove_task(self, task_id):
        if task_id not in self.all_tasks:
            raise ValueError("There is no task with {} id".format(task_id))
        if self.all_tasks[task_id].parent_id:
            parent_id = self.all_tasks[task_id].parent_id
            self.all_tasks[parent_id].subtasks.remove(task_id)
        self.remove_helper(task_id)


    def remove_helper(self, task_id):
        if task_id and task_id in self.all_tasks:
            sub_ids = self.all_tasks[task_id].subtasks
            for sub_t_id in sub_ids:
                self.remove_helper(sub_t_id)
            sub_ids.clear()
            del self.all_tasks[task_id]

    def container_check_overdue(self):
        to_remove = []
        for task in self.all_tasks.values():
            if task.task_mark_overdue():
                #print(self.mark_as_failed(task.id))
                to_remove.extend(self.mark_as_failed(task.id))

            else:
                continue
        for id in to_remove:
            self.container_remove_task(id)

    def mark_as_failed(self, task_id):
        if task_id and task_id in self.all_tasks:
            self.fail_task(task_id)
            if not self.all_tasks[task_id].recurring:
                #to_remove.append(task_id)
                yield task_id
            else:
                self.all_tasks[task_id].task_rearrange()

    def fail_task(self, task_id):
        if task_id and task_id in self.all_tasks:
            self.failed_tasks[task_id] = copy.deepcopy(self.all_tasks[task_id])
            sub_ids = self.all_tasks[task_id].subtasks
            for sub_t_id in sub_ids:
                self.fail_task(sub_t_id)
                self.failed_tasks[sub_t_id] = copy.deepcopy(self.all_tasks[sub_t_id])

    def mark_as_completed(self, task_id):
        if task_id and task_id in self.all_tasks:
            parent_id = self.all_tasks[task_id].parent_id

            if parent_id and not self.all_tasks[task_id].recurring:
                self.all_tasks[parent_id].subtasks.remove(task_id)

            self.complete_task(task_id)
            if not self.all_tasks[task_id].recurring:
                self.container_remove_task(task_id)
        else:
            raise ValueError("There is no {} id".format(task_id))

    def complete_task(self, task_id):
        if task_id and task_id in self.all_tasks:
            self.all_tasks[task_id].task_complete()
            self.completed_tasks[task_id] = copy.deepcopy(self.all_tasks[task_id])

            if self.all_tasks[task_id].recurring:
                self.all_tasks[task_id].task_rearrange()
                self.all_tasks[task_id].task_check_end()

            sub_ids = self.all_tasks[task_id].subtasks
            for sub_t_id in sub_ids:
                self.complete_task(sub_t_id)
                self.completed_tasks[sub_t_id] = copy.deepcopy(self.all_tasks[sub_t_id])
        else:
            raise ValueError("There is no task with such id{}".format(task_id))

    def container_move_task(self, source_id, destination_id):
        if source_id not in self.all_tasks:
            raise ValueError("There is no {} source id".format(source_id))

        if destination_id:
            if source_id == destination_id:
                raise ValueError("Ids are the same")

            if destination_id not in self.all_tasks:
                raise ValueError("There is no {} id".format(destination_id))

            temp_id = destination_id
            while temp_id:
                temp_id = self.all_tasks[temp_id].parent_id
                if temp_id == source_id:
                    raise ValueError("You cannot move task to it's subtasks")

            if self.all_tasks[source_id].parent_id:
                parent_id = self.all_tasks[source_id].parent_id
                self.all_tasks[parent_id].subtasks.remove(source_id)

            self.all_tasks[destination_id].subtasks.append(source_id)
            self.all_tasks[source_id].parent_id = destination_id
        else:
            if self.all_tasks[source_id].parent_id:
                parent_id = self.all_tasks[source_id].parent_id
                self.all_tasks[parent_id].subtasks.remove(source_id)
                self.all_tasks[source_id].parent_id = None
            else:
                raise ValueError("Task is already root task")




    def container_task_edit(self,task_id, name=None, priority=None, interval=None):
        if task_id in self.all_tasks:
            self.all_tasks[task_id].task_edit(name=name, priority=priority, interval=interval)
        else:
            raise ValueError("There is no task with {} id".format(task_id))

    def show_current(self):
        for task in self.all_tasks.values():
            print(task)

    def show_done(self):
        for task in self.completed_tasks.values():
            print(task)

    def show_failed(self):
        for task in self.failed_tasks.values():
            print(task)


    def __str__(self):
        all = [str(x) for x in self.all_tasks.values()]
        completed = [str(x) for x in self.completed_tasks.values()]
        failed = [str(x) for x in self.failed_tasks.values()]
        s = "\n\t".join(["Project tasks:", "Active:", *all, "Completed:", *completed, "Failed:", *failed])
        return s



