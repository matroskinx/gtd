from dateutil.relativedelta import *
import re


class TimeDelta:
    intervals = {"hourly":relativedelta(hours=1),
                 "daily": relativedelta(days=1),
                 "weekly": relativedelta(days=7),
                 "monthly": relativedelta(months=1),
                 "yearly": relativedelta(years=1)}


def get_time_delta(period):
    if not period:
        return None

    p = re.compile(r"((?:[1-9][hdwmy])|(?:[1-9][0-9]+[hdwmy]))|(daily|hourly|weekly|monthly|yearly)")
    e = re.search(p, period)
    if not e:
        raise ValueError("No match")

    if e.groups()[0]:
        s = e.groups()[0]
        if s.endswith("h"):
            delta = relativedelta(hours=1)
        elif s.endswith("d"):
            delta = relativedelta(days=1)
        elif s.endswith("w"):
            delta = relativedelta(days=7)
        elif s.endswith("m"):
            delta = relativedelta(months=1)
        elif s.endswith("y"):
            delta = relativedelta(years=1)
        else:
            raise ValueError("No match")
        s = s[:-1]
        num = int(s)
        delta = delta * num
        return delta

    elif e.groups()[1]:
        s = e.groups()[1]
        return TimeDelta.intervals.get(s)


def get_recurrence_days(str_days):
    if not str_days:
        return None
    pattern = re.compile(r"(mon|tue|wed|thu|fri|sat|sun)")
    e = re.findall(pattern, str_days)
    if not e:
        raise ValueError("Unable to parse days")

    days = []

    li = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"]

    for y, x in enumerate(li):
        if x in e:
            days.append(y)

    return days

