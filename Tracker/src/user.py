import itertools
import logging


class User:
    uidGen = itertools.count(start=1)

    def __init__(self, name):
        self.uid = str(next(self.uidGen))
        self.name = name
        self.project_ids = []

    def change_name(self, new_name):
        self.name = new_name

    def __str__(self):
        s = " | ".join([self.uid, self.name])
        return s