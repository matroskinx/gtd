import itertools
import jsonpickle
from datetime import datetime
from dateutil.parser import *

from src import Container
from src import User
from src import Task, Priority
from src.settings import Settings
from src.project import Project
from src.utils import *
import os
import logging


class Repository:
    """
    Store pairs User-Container as dict.
    Save and load containers for specific users.
    Add and delete users.
    """
    def __init__(self, config_name=None):
        self.repo = {}
        self.current_user = None
        self.users = {}
        self.projects = []
        self.settings = Settings(config_name)
        self.logger = self.set_logger()
        # logging.basicConfig(format="%(asctime)s | %(message)s | %(module)s", datefmt='%m/%d/%Y %H:%M:%S',
        #                     filename="log.log", level=logging.DEBUG)
        # logging.info("Program started")

    def set_logger(self):
        logger = logging.getLogger("repository")
        logger.setLevel(logging.INFO)
        logger.setLevel(logging.ERROR)
        logger.setLevel(logging.DEBUG)
        fh = logging.FileHandler("log.log")

        formatter = logging.Formatter('%(asctime)s | %(name)s | %(levelname)s | %(message)s',
                                      datefmt="%m/%d/%Y %H:%M:%S")
        fh.setFormatter(formatter)

        logger.addHandler(fh)
        return logger

    def add_user(self, name):
        usr = User(name)
        self.users[usr.uid] = usr
        self.repo[usr.uid] = Container()
        self.current_user = usr
        print("AAAAAA {} AAAAAA".format(name))
        self.logger.info("Added user %s", name)
        # Repository.switch_user(self, name)
        return usr.uid

    def remove_user(self, name):
        uid = Repository.get_uid(self, name)
        if uid in self.users:
            del self.users[uid]
            self.logger.info("Removed user %s", name)
        else:
            self.logger.error("Failed to remove user with name %s", name)
            raise NameError("There is no user with name %s", name)

    def get_uid(self, name):
        """
        Returns uid of given user name.
        If name is not found, raises exception
        """
        for key in self.users:
            if self.users[key].name == name:
                return self.users[key].uid
        # return self.current_user.uid
        self.logger.error("Failed get id of a user with name %s", name)
        raise NameError("There is no user with name %s", name)

    def switch_user(self, user_name):
        uid = Repository.get_uid(self, user_name)
        if uid == self.current_user.uid:
            self.logger.error("Failed to switch to user with name %s", user_name)
            raise NameError("You are already in")
        self.current_user = self.users.get(uid)
        self.logger.info("Switched to user with name %s", user_name)

    def save_current_user(self):
        p = os.path.join(self.settings.folder, self.settings.current_user)
        t = jsonpickle.encode(self.current_user)
        with open(p, "w") as file:
            file.write(t)

    def load_current_user(self):
        p = os.path.join(self.settings.folder, self.settings.current_user)
        if os.path.exists(p):
            if os.path.getsize(p) == 0:
                raise ValueError("Current user file is damaged")
            with open(p, "r") as file:
                self.current_user = jsonpickle.decode(file.read())
        else:
            raise FileNotFoundError("There is no current user file")


    def save_users(self):
        self.users[self.current_user.uid] = self.current_user
        t = jsonpickle.encode(self.users)
        p = os.path.join(self.settings.folder, self.settings.users)
        with open(p, "w") as file:
            file.write(t)

    def load_users(self):
        p = os.path.join(self.settings.folder, self.settings.users)
        if not os.path.exists(self.settings.folder):
            raise FileNotFoundError("There is no data folder, looks like you need to run init")

        if os.path.exists(p):
            if os.path.getsize(p) == 0:
                raise ValueError("Users file is damaged")
            with open(p, "r") as file:
                self.users = jsonpickle.decode(file.read())
        else:
            raise FileNotFoundError("There is no users file")


    def save_repo(self):
        p = os.path.join(self.settings.folder, self.settings.repository)
        with open(p, "w") as file:
            t = jsonpickle.encode(self.repo)
            file.write(t)

    def save_projects(self):
        p = os.path.join(self.settings.folder, self.settings.projects)
        with open(p, "w") as file:
            t = jsonpickle.encode(self.projects)
            file.write(t)

    def load_projects(self):
        p = os.path.join(self.settings.folder, self.settings.projects)
        if os.path.exists(p):
            if os.path.getsize(p) == 0:
                raise ValueError("Projects file is damaged")
            with open(p, "r") as file:
                self.projects = jsonpickle.decode(file.read())
        else:
            raise FileNotFoundError("There is no projects file")

    def load_repo(self):
        p = os.path.join(self.settings.folder, self.settings.repository)
        if os.path.exists(p):
            if os.path.getsize(p) == 0:
                raise ValueError("Repository file is damaged")
            with open(p, "r") as file:
                self.repo = jsonpickle.decode(file.read())
        else:
            raise FileNotFoundError("There is no repository file")

    def add_task_to_user(self, task_name, project_id=None, task_priority=Priority.Medium,
                         task_interval=None, task_parent_id=None, task_days=None,
                         task_start_time=None, task_end_time=None, task_due_date=None):
        """
        Add task to current user.
        If user_name is not specified, add task to current user.
        If task_parent_id is specified, add task as subtask.
        Specify project_name to add task to project, else it'll be added to task of current_user
        :param task_name task name
        :param project_id task will be added to all tasks if project is not specified
        """
        if task_days and task_interval:
            self.logger.error("Failed to add task")
            raise ValueError("Choose between interval and recurrence days")

        if (task_interval or task_days) and not task_due_date:
            self.logger.error("Failed to add task")
            raise ValueError("You must specify due date for recurring tasks")

        if task_end_time and (task_interval or task_days) and task_due_date:
            task_end_time = parse(task_end_time)
        elif task_end_time and not (task_interval or task_days):
            self.logger.error("Failed to add task")
            raise ValueError("There is no need to specify end time for non-recurring tasks")

        interval = get_time_delta(task_interval)
        task_days = get_recurrence_days(task_days)
        uid = self.current_user.uid

        if task_parent_id and task_parent_id not in self.repo[uid].all_tasks:
            self.logger.error("Failed to add task")
            raise NameError("There is no parent with id %s", task_parent_id)

        if task_due_date:
            due = parse(task_due_date)
            if due > datetime.now():
                t = Task(task_name, priority=task_priority, interval=interval, parent_id=task_parent_id,
                         start_time=task_start_time, end_time=task_end_time, due_date=due, recur_days=task_days)
                # self.repo[uid].container_add_task(t)
            else:
                self.logger.error("Failed to add task")
                raise ValueError("Due date cannot be in the past")
        else:
            t = Task(task_name, priority=task_priority, interval=interval, parent_id=task_parent_id,
                     start_time=task_start_time, end_time=task_end_time, due_date=None)
            # self.repo[uid].container_add_task(t)
        if project_id:
            p = self.get_project(project_id)
            p.storage.container_add_task(t)
            self.logger.info("Added task %s to project %s", t.id, p.pid)
            return t.id
        else:
            self.repo[uid].container_add_task(t)
            self.logger.info("Added task %s to user %s", t.id, uid)
            return t.id

    def complete_user_task(self, task_id, project_id=None):
        uid = self.current_user.uid

        if project_id:
            p = self.get_project(project_id)
            p.storage.mark_as_completed(task_id)
            self.logger.info("Completed task %s by user %s in project %s", task_id.id, uid, p.pid)
        else:
            self.repo[uid].mark_as_completed(task_id)
            self.logger.info("Completed task %s by user %s", task_id, uid)

    def check_user_overdue(self):
        container = self.repo[self.current_user.uid]
        container.container_check_overdue()

    def check_projects_overdue(self):
        for project in self.projects:
            project.storage.container_check_overdue()

    def remove_task(self, task_id, project_id=None):
        uid = self.current_user.uid
        if project_id:
            p = self.get_project(project_id)
            p.storage.container_remove_task(task_id)
            self.logger.info("Removed task %s by user %s from project %s", task_id.id, uid, p.pid)
        else:
            self.repo[uid].container_remove_task(task_id)
            self.logger.info("Removed task %s by user %s", task_id.id, uid)

    def init_repo(self, user_name):
        """
        Initializes repository, creates data folder.
        If repository already exists, call to this function will overwrite existing files.
        """

        if not os.path.exists(self.settings.folder):
            os.mkdir(self.settings.folder)

        p = os.path.join(self.settings.folder, self.settings.users)
        with open(p, "w") as file:
            file.write("null")

        p = os.path.join(self.settings.folder, self.settings.current_user)
        with open(p, "w") as file:
            file.write("null")

        p = os.path.join(self.settings.folder, self.settings.projects)
        with open(p, "w") as file:
            file.write("null")

        p = os.path.join(self.settings.folder, self.settings.repository)
        with open(p, "w") as file:
            file.write("null")

        p = os.path.join(self.settings.folder, self.settings.ids)
        with open(p, "w") as file:
            file.write("1 1 1")

        self.add_user(user_name)

        self.stop()
        # self.save_current_user()
        # self.save_users()
        # self.save_repo()
        # self.save_projects()

    def clear_repo(self):
        pass

    def run(self):
        Repository.load_users(self)
        Repository.load_current_user(self)
        Repository.load_repo(self)
        Repository.load_projects(self)

        p = os.path.join(self.settings.folder, self.settings.ids)
        if os.path.exists(p):
            if os.path.getsize(p) == 0:
                raise ValueError("Ids file is damaged")
            with open(p, "r") as file:
                s = file.read()
                s = s.split()
                Task.idGen = itertools.count(start=int(s[0]))
                User.uidGen = itertools.count(start=int(s[1]))
                Project.pidGen = itertools.count(start=int(s[2]))
        else:
            raise FileNotFoundError("There is no ids file")
        self.logger.debug("Loaded")
        self.check_user_overdue()
        self.check_projects_overdue()
        self.logger.debug("Checked overdue")

    def stop(self):
        Repository.save_users(self)
        Repository.save_current_user(self)
        Repository.save_repo(self)
        Repository.save_projects(self)
        self.logger.debug("Saved repository")
        p = os.path.join(self.settings.folder, self.settings.ids)
        with open(p, "w") as file:
            s = str(next(Task.idGen)) + " " + str(next(User.uidGen)) + " " + str(next(Project.pidGen))
            file.write(s)

    def get_project(self, project_id):
        for p in self.projects:
            if p.pid == project_id:
                return p
        else:
            self.logger.error("Failed to get project with {} id".format(project_id))
            raise ValueError("There is no project with {} id".format(project_id))

    def create_project(self, name):
        cr_id = self.current_user.uid
        p = Project(name, cr_id)
        p.user_ids.append(cr_id)
        self.projects.append(p)
        self.current_user.project_ids.append(p.pid)
        self.logger.info("Created project %s", p.pid)
        return p.pid

    def move_task_to_project(self, task_id, project_id):
        uid = self.current_user.uid
        cnt = self.repo[uid].all_tasks
        if task_id and task_id in cnt:
            if cnt[task_id].parent_id:
                self.logger.error("Failed to move task %s to project %s", task_id, project_id)
                raise ValueError("You can only move/add root tasks")
            else:
                p = self.get_project(project_id)
                if p.pid not in self.users[uid].project_ids:
                    self.logger.error("User %s failed to access project %s", uid, project_id)
                    raise ValueError("There is no such project that belongs to you")

                to_move = self.__collect_tasks__(task_id, cnt)
                for id in to_move:
                    p.storage.all_tasks[id] = cnt[id]
                    del cnt[id]
                self.logger.info("Moved task %s to project %s", task_id, project_id)
        else:
            self.logger.error("Failed to move task %s to project %s", task_id, project_id)
            raise NameError("There is no task with {} id".format(task_id))

    def move_task_from_project(self, project_id, task_id):
        uid = self.current_user.uid
        p = self.get_project(project_id)
        if p.pid not in self.current_user.project_ids:
            raise NameError("There is no such project that belongs to you")
        else:
            cnt = p.storage.all_tasks
            if task_id in cnt:
                if cnt[task_id].parent_id:
                    raise ValueError("You can only move/add root tasks")
                else:
                    to_move = self.__collect_tasks__(task_id, cnt)
                    for id in to_move:
                        self.repo[uid].all_tasks[id] = cnt[id]
                        del cnt[id]
            else:
                raise NameError("There is no task with %s id in project %s", task_id, project_id)

    def __collect_tasks__(self, task_id, container):
        collected = [task_id]
        # collected = []
        # collected.append(task_id)
        for t_id in container[task_id].subtasks:
            collected.extend(self.__collect_tasks__(t_id, container))

        self.logger.debug("Collected tasks {}".format(*collected))
        return collected

    def invite_users(self, project_id, user_name):
        """
        Invite users to current user project.
        """
        if project_id not in self.current_user.project_ids:
            self.logger.error("User %s failed to access project %s", user_name, project_id)
            raise NameError("There is no such project that belongs to you")
        else:
            project = self.get_project(project_id)
            uid = self.get_uid(user_name)
            if uid is not self.current_user.uid:
                project.user_ids.append(uid)
                self.users[uid].project_ids.append(project.pid)
            else:
                self.logger.error("User %s failed to invite itself", user_name)
                raise NameError("You cannot invite yourself")

    def leave_project(self, project_id):
        uid = self.current_user.uid
        if project_id in self.current_user.project_ids:
            p = self.get_project(project_id)
            p.remove_user(uid)
            self.current_user.project_ids.remove(project_id)
        else:
            self.logger.error("User %s failed to leave project %s", uid, project_id)
            raise ValueError("There is no such project")

    def move_task(self, source_id, destination_id, project_id=None):
        uid = self.current_user.uid

        if project_id:
            p = self.get_project(project_id)
            p.storage.container_move_task(source_id, destination_id)
        else:
            self.repo[uid].container_move_task(source_id, destination_id)

    def edit_task(self, task_id, project_id=None, name=None, priority=None, interval=None):
        uid = self.current_user.uid

        interval = get_time_delta(interval)

        if project_id:
            p = self.get_project(project_id)
            p.storage.container_task_edit(name=name, priority=priority, interval=interval)
        else:
            self.repo[uid].container_task_edit(task_id, name=name, priority=priority, interval=interval)

    def print_tasks(self):
        uid = self.current_user.uid
        self.repo[uid].show_current()

    def print_done_tasks(self):
        uid = self.current_user.uid
        self.repo[uid].show_done()

    def print_failed_tasks(self):
        uid = self.current_user.uid
        self.repo[uid].show_failed()

    def print_projects(self):
        project_ids = self.current_user.project_ids
        for p in self.projects:
            if p.pid in project_ids:
                g = [self.users[x].name for x in p.user_ids]
                print("\n", p, sep="", end=" ")
                print("Users: ", ", ".join([*g]))
                print(p.storage)

