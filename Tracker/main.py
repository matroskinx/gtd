import jsonpickle

from src import Container
from src import Repository
from src import User
from src import Task


def main():
    association = {}
    admin = User("ADMIN")
    cnt = Container()
    association[admin] = cnt  # ChainMap
    a = Task("Simple task")

    b = Task("Second task")
    c = Task("Third task")

    a.AddSubtasks(b, c)
    cnt.addTask(a)
    cnt.addTask(b)
    # for key in cnt.all_tasks:
    #     print(cnt.all_tasks[key].id, " ", cnt.all_tasks[key].name, " ", cnt.all_tasks[key].completed)
    #     cnt.all_tasks[key].MarkAsCompleted()
    print()
    for key in cnt.all_tasks:
        print(cnt.all_tasks[key].id, " ", cnt.all_tasks[key].name, " ", cnt.all_tasks[key].completed)

    cnt.removeTask(c)

    print("was")

    for key in cnt.all_tasks:
        print(cnt.all_tasks[key].id, " ", cnt.all_tasks[key].name, " ", cnt.all_tasks[key].completed)
        for k in cnt.all_tasks[key].subtasks:
            print("\t", cnt.all_tasks[key].subtasks[k].name)

    frozen = jsonpickle.encode(cnt)

    # with open("collec","w") as file:
    #    file.write(frozen)

    # with open("collec", "r") as file:
    #     rest = jsonpickle.decode(file.read())

    # print("restored")
    # for key in rest.all_tasks:
    #     print(rest.all_tasks[key].id, " ", rest.all_tasks[key].name, " ", rest.all_tasks[key].completed)
    #     for k in rest.all_tasks[key].subtasks:
    #         print("\t", rest.all_tasks[key].subtasks[k].name)


if __name__ == "__main__":
    main()